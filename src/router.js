import Vue from "vue";
import Router from "vue-router";
import AppHeader from "./layout/AppHeader";
import AppFooter from "./layout/AppFooter";
import Components from "./views/Components.vue";
import Landing from "./views/Landing.vue";
import Login from "./views/auth/Login.vue";
import Register from "./views/auth/Register.vue";
import Profile from "./views/Profile.vue";
import Navbar from "./views/menu/Navbar.vue";

import Home from "./views/user/Home.vue";
import List from "./views/movie/List.vue";


Vue.use(Router);

export default new Router({
  linkExactActiveClass: "active",
  routes: [
    {
      path: "/",
      name: "index",
      components: {
        default: Login,
      },
      meta: {requiresVisitor: true}
    },
    {
      path: "/register",
      name: "register",
      components: {
        default: Register,
      },
      meta: {requiresVisitor: true}
    },
    {
      path: "/login",
      name: "login",
      components: {
        default: Login,
      },
      meta: {requiresVisitor: true}
    },
    {
      path: "/home",
      name: "home",
      components: {
        header: Navbar,
        default: Home,

      },
      meta: {requiresVisitor: true}
    },
    {
      path: "/list",
      name: "list",
      components: {
        default: List,

      },
      meta: {requiresVisitor: true}
    },


    {
      path: "/landing",
      name: "landing",
      components: {
        header: AppHeader,
        default: Landing,
        footer: AppFooter
      }
    },



  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});
