/*!

=========================================================
* Vue Argon Design System - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system/blob/master/LICENSE.md)

* Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Argon from "./plugins/argon-kit";
import './registerServiceWorker'
import vueHeadful from 'vue-headful';
import VueCurrencyFilter from 'vue-currency-filter'
import Toasted from 'vue-toasted'
import Tooltip from 'vue-directive-tooltip';
import map from 'lodash/map'
import { ValidationProvider } from 'vee-validate';
import VueLodash from 'vue-lodash'


import {store} from './store/api'

const options = { name: 'lodash' } // customize the way you want to call it

Vue.use(VueLodash, options) // options is optional

Vue.prototype.$eventHub = new Vue();

Vue.component('vue-headful', vueHeadful);

Vue.use(Toasted)

Vue.component('ValidationProvider', ValidationProvider);

Vue.filter('formatValue', function (value) {
  let newValue = value.toString()
  return newValue.replace('.', '').replace('.', '').replace(',', '.');
});


Vue.mixin({
  methods: {
    handleErrors: function (response) {
      let toasted = this.$toasted;
      if (response.status === 401 || response.data.status === 401) {
        toasted.show("Acesso negado, confira seus dados!", {
          position: 'top-center',
          type: "error"
        }).goAway(5000)
        this.$router.push({name: 'login'})
      } else {
        if (response.data.message !== 'The given data was invalid.') {
          toasted.show(response.data.message, {
            position: 'top-center',
            type: "error"
          }).goAway(5000)
        }
        if (response.data.errors) {
          let errors = response.data.errors
          map(errors, function (value, key) {
            toasted.show(value, {
              position: 'top-center',
              type: "error"
            }).goAway(5000)
          });
        }
      }
    },
  }
});

if (process.env.NODE_ENV !== "local") {
  Vue.config.devtools = true
  Vue.config.debug = true
  Vue.config.silent = true
}

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.loggedIn) {
      next({
        name: 'login'
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.requiresVisitor)) {
    if (store.getters.loggedIn) {
      next()
    } else {
      next()
    }
  } else {
    next()
  }
})



Vue.use(Tooltip, {
  delay: 500,
  placement: 'top',
  triggers: ['hover'],
  offset: 0
});



Vue.config.productionTip = false;
Vue.use(Argon);





new Vue({
  router,
  store: store,
  render: h => h(App)
}).$mount("#app");
