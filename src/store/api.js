import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

//axios.defaults.baseURL = 'http://movies.api/api/'

axios.defaults.baseURL = process.env.API_ROOT

export const store = new Vuex.Store({
    state: {
        token: (localStorage.getItem('access_token') && localStorage.getItem('access_token') !== 'undefined') ? localStorage.getItem('access_token') : null,
        user: {
            name: 'carregando...',
            level: '-'
        }
    },
    getters: {
        loggedIn(state) {
            return state.token !== null
        },
        user(state) {
            return state.user
        },
    },
    mutations: {
        retrieveToken(state, token) {
            state.token = token
        },
        destroyToken(state) {
            state.token = null
        },
        retrieveUser(state, user) {
            state.user = user
        },
    },
    actions: {
        register(context, credentials) {
            return new Promise(async (resolve, reject) => {
                await axios.post('/register', credentials)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        login(context, credentials) {
            return new Promise(async (resolve, reject) => {
                await axios.post('/auth/login', {
                    email: credentials.email,
                    password: credentials.password,
                })
                    .then(response => {
                        const token = response.data.access_token
                        localStorage.setItem('access_token', token)
                        context.commit('login', token)
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        retrieveUser(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            axios.get('/auth/user')
                .then(response => {
                    context.commit('retrieveUser', response.data.user)
                })
                .catch(error => {
                    if (error.response.status === 401) {
                        localStorage.removeItem('access_token')
                        context.commit('destroyToken')
                        this.$router.push({name: 'login'})
                    }
                    console.log(error.response)
                })
        },
        retrieveToken(context, credentials) {
            return new Promise(async (resolve, reject) => {
                await axios.post('/auth/login', {
                    username: credentials.username,
                    password: credentials.password,
                    recaptcha: credentials.recaptcha,
                })
                    .then(response => {
                        const token = response.data.access_token
                        localStorage.setItem('access_token', token)
                        context.commit('retrieveToken', token)
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        destroyToken(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            if (context.getters.loggedIn) {
                return new Promise(async (resolve, reject) => {
                    await axios.get('/logout')
                        .then(response => {
                            localStorage.removeItem('access_token')
                            context.commit('destroyToken')
                            resolve(response)
                        })
                        .catch(error => {
                            localStorage.removeItem('access_token')
                            context.commit('destroyToken')
                            reject(error)
                        })
                })
            }
        },
        senderMovie(context, user) {
            return new Promise(async (resolve, reject) => {
                await axios.post('/auth/movie/create', user)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        getMovies(context, user) {
            return new Promise(async (resolve, reject) => {
                await axios.get('/auth/movies', user)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        senderTag(context, user) {
            return new Promise(async (resolve, reject) => {
                await axios.post('/auth/tag/create', user)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        getTags(context, user) {
            return new Promise(async (resolve, reject) => {
                await axios.get('/auth/tags', user)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        getTagMovie(context, user) {
            return new Promise(async (resolve, reject) => {
                await axios.post('/auth/tags/movie/', user)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        deleteTagMovie(context, document) {
            return new Promise(async (resolve, reject) => {
                await axios.get('/auth/tags/movie/delete/' + document)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        senderTagMovie(context, user) {
            return new Promise(async (resolve, reject) => {
                await axios.post('/auth/movie/tags/insert', user)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        updateMovie(context, user) {
            return new Promise(async (resolve, reject) => {
                await axios.post('/auth/movie/update', user)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        deleteMovie(context, document) {
            return new Promise(async (resolve, reject) => {
                await axios.get('/auth/movie/delete/' + document)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
    }
});